# Heimdall
Heimdall is a python app that fetches SSL certificates information from devices in its inventory and stores it in a database. 
Subsequently, Heimdall identifies if a NOTIFICATION should be raised for polled certificates and call a third-party notification script/API to notify necessary team(s).

_**IMPORTANT:** Currently, Heimdall supports Cisco VOS and Microsoft Windows OS only._

### Installation
since this is a pure python app, the only requirement is to have python 3.6+ and pip3 in the system.
With those conditions satisfied, `pip install -r requirements.txt` can be run to pull external necessary libraries.

### Inventory
To let the app know of devices in-scope and how to connect to them, an "inventory" folder must exist in the root. Inside that folder, up to two files may exist: 
 - hosts.yaml (mandatory): contains information about each individual host (see Figure 1). It is necessary to group servers under a 'vendor' group, so that Heimdall knows what method to use (interface) to connect and parse SSL information from a particular host. Since we are working with Cisco only at the moment, that's the only vendor group in the hosts.yaml file right now. Usernames, passwords and SSH connection details could also be informed in the hosts.yaml file, but because all servers use the same credentials it would be just repetitive. To optimize for such cases, Heimdall supports default parameters input via a separate file;
 - defaults.yaml (optional): Parameters here must also be organized under a vendor group. A (key:value) pair under a vendor group in defaults.yaml will be applied to each host under the same vendor group in hosts.yaml, unless the host in hosts.yaml already contains the same key assigned, in which case it gets precedence over what is defined in defaults.yaml. 

The overall inventory loading logic is as follows:
 - First, Heimdall will load the information in hosts.yaml file into a dictionary data structure. Parameters coming from this file get precedence. 
 - Once that's done, it will look at defaults.yaml and add whatever parameters informed under a vendor group to each server under that same vendor group. Parameters loaded from defaults.yaml do not overwrite parameters already loaded in memory from hosts.yaml. (For example, suppose a particular host uses a different TCP port for the SSH connection. That port could be informed under the respective host in the hosts.yaml file and it wouldn't be overwritten by the default port informed in defaults.yaml. The port in defaults.yaml would then be used to all other servers that didn't have an explicit port in hosts.yaml)

The expected inventory assets structure is identified below:

```
.
├── inventory
│   ├── defaults.yaml
│   └── hosts.yaml
```
_OBS: The app is nicknamed "HEIMDALL", after the Norse watchman god with keen vision._

### Settings
In `./settings.py` app global settings are identified below. Current values represent the defaults:

```
sql_alchemy = {
    'db_uri': 'sqlite:///data.db'                                   <<< URI of database to store SSL cert info
}

notifications = {
    'script': '/opt/cnmc/eircom/cell-messenger/bin/cell-event',     <<< path to notification script
    'notify_delta': 30,                                             <<< number of days before expiration to starte generating notification
    'severity': {
        30: 'MAJOR',                                                <<< number of days before expiration to generate MAJOR level notification
        90: 'CRITICAL',                                             <<< number of days before expiration to generate CRITICAL level notification
        180: 'WARN'                                                 <<< number of days before expiration to generate WARN level notification
    }
}

multithreading = {
    'max_workers': 20                                               <<< indicates the max number of child-threads started by the app during polling pahse
}

```

### Usage
once the app is installed and settings.py is correctly configure to your environment, to execute the app, run the following command in the app's root dir:

`python heimdall.py`


import re
import paramiko
from .abstract import AbstractInterface
from datetime import datetime


class SSHConnection(object):
    def __init__(self, hostname, ip, port, username, password, look_for_keys=False, allow_agent=False):
        self.hostname = hostname
        self.ip = ip
        self.port = port
        self.username = username
        self.password = password
        self.look_for_keys = look_for_keys
        self.allow_agent = allow_agent
        self.ssh_client = paramiko.SSHClient()
        # this attribute is just to store logs produced during the CLI section for troubleshooting purposes.
        self.logs = ''

    def connect(self):
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh_client.connect(hostname=self.ip, port=self.port, username=self.username, password=self.password,
                                look_for_keys=self.look_for_keys, allow_agent=self.allow_agent)
        if self.ssh_client.get_transport().is_active():
            self.logs += 'Connected to host {} (ip:{})\n'.format(self.hostname, self.ip)

    def disconnect(self):
        self.logs += 'Closing connection with {} (ip: {})\n'.format(self.hostname, self.ip)
        self.ssh_client.close()

    def scan_certificates(self):
        """
        This function painfully manages the CLI session with the device and parses the outputs.
        It's a snow flake... if anything other than the expected output is printed on the screen, this mammoth crumbles
        on itself.

        :return: a list of dictionaries of certificates names with their respective expiration date. Example:
            [{
              "cert_name_1": {
                "expires_at": "2023-01-08"
              }
            },
            {
              "cert_name_2": {
                "expires_at": "2026-01-27"
              }
            }]

        This list is then made the value to "ssl_certs" key within CiscoInterface inventory attribute.
        """
        self.connect()
        shell = self.ssh_client.invoke_shell()
        self.logs += 'waiting CLI to startup\n'
        if shell.send_ready():
            while True:
                if '{}:'.format(self.username) in str(shell.recv(10000).decode('utf-8')):
                    self.logs += 'collecting list of ssl certificates\n'
                    shell.send('show cert list own\n')
                    while True:
                        show_cert_list_own_output = str(shell.recv(1000000).decode('utf-8'))
                        if 'by' in show_cert_list_own_output:
                            cert_list_pattern = re.compile(r'([\w-]+?)/.*')
                            cert_list_matches = cert_list_pattern.finditer(show_cert_list_own_output)
                            cert_list = []
                            for cert in cert_list_matches:
                                self.logs += 'extracting expiration date of cert {}\n'.format(cert.group(1))
                                shell.send('show cert own {}\n'.format(str(cert.group(1))))
                                while True:
                                    show_cert_own_certificate_output = str(shell.recv(1000000).decode('utf-8'))
                                    if '<q> to quit' in show_cert_own_certificate_output:
                                        expires_at_pattern = re.compile(r'To: (.+)')
                                        expires_at_match = expires_at_pattern.finditer(show_cert_own_certificate_output)
                                        expires_at = next(x for x in expires_at_match).group(1).lstrip().rstrip()
                                        self.logs += '=====> {}\n'.format(expires_at)
                                        expires_at = expires_at.split(" ")
                                        expires_at.pop(4)
                                        cert_list.append({cert.group(1):
                                                              {'expires_at': datetime.strptime(" ".join(expires_at),
                                                                                               "%a %b %d %H:%M:%S %Y").date()}
                                                          })
                                        shell.send('q\n')
                                        break
                            self.disconnect()
                            self.logs += '*' * 50 + '\n'
                            return cert_list


class CiscoInterface(AbstractInterface):
    def probe(self, host):
        """
        docstring provided in AbstractClass
        """
        ssh_connection = SSHConnection(hostname=host, **self.inventory[host])
        self.inventory[host]['ssl_certs'] = ssh_connection.scan_certificates()
        return ssh_connection.logs

import re
import subprocess
from .abstract import AbstractInterface
from datetime import datetime


class HTTPRequest(object):
    def __init__(self, hostname, ip, port=80, username=None, password=None):
        self.hostname = hostname
        self.ip = ip
        self.port = port
        self.url = 'https://{}/'.format(self.ip)
        self.logs = ''
        # we will probably not use any credentials for HTTP
        self.username = username
        self.password = password

    def scan_certificates(self):
        curl_process = subprocess.Popen(["curl", "--insecure", "-vvI", self.url],
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)

        self.logs += "Polling certificate info from {} (ip: {})\n".format(self.hostname, self.ip)
        _, verbose_logs = curl_process.communicate()

        output = str(verbose_logs.decode('utf-8'))
        cert_name_pattern = re.compile(r'issuer: .*; CN=(.+); email')
        cert_name_match = cert_name_pattern.finditer(output)
        expires_at_pattern = re.compile(r'expire date: (.+)')
        expires_at_match = expires_at_pattern.finditer(output)
        cert_list = []
        for cert in cert_name_match:
            expires_at = next(x for x in expires_at_match).group(1).lstrip().rstrip()
            self.logs += "Expiration date for cert {}\n".format(cert.group(1))
            self.logs += '=====> {}\n'.format(expires_at)
            expires_at = expires_at.split(" ")
            expires_at.pop(-1)
            cert_list.append({cert.group(1): {
                "expires_at": datetime.strptime(" ".join(expires_at), "%b %d %H:%M:%S %Y").date()}
            })
        self.logs += ('*' * 50) + '\n'
        return cert_list


class MicrosoftInterface(AbstractInterface):
    def probe(self, host):
        """
        docstring provided in AbstractClass
        """
        http_req = HTTPRequest(hostname=host, **self.inventory[host])
        self.inventory[host]['ssl_certs'] = http_req.scan_certificates()
        return http_req.logs

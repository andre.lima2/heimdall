import os
import re
import yaml
import json
import logging
from dotenv import load_dotenv
from interfaces.cisco import CiscoInterface
from interfaces.microsoft import MicrosoftInterface
from utils.db import engine, session, Base
from utils.whistleblower import Whistleblower
from models.certificate import Certificate


INTERFACES = {
    'cisco': CiscoInterface,
    'microsoft': MicrosoftInterface
}


class Heimdall(object):
    def __init__(self, inventory_dir='{}/inventory'.format(os.getcwd()),
                 db_engine=engine, db_session=session, db_base=Base):
        load_dotenv()
        self.inventory_dir = inventory_dir
        self.db = {'session': db_session, 'engine': db_engine, 'base': db_base}
        self.defaults = self.load_defaults()
        self.inventory = self.load_inventory()

    def load_defaults(self):
        with open('{}/defaults.yaml'.format(self.inventory_dir)) as defaults_file:
            defaults = yaml.load(defaults_file, Loader=yaml.FullLoader)
        return defaults

    def load_inventory(self):
        with open('{}/hosts.yaml'.format(self.inventory_dir)) as inventory_file:
            inventory = yaml.load(inventory_file, Loader=yaml.FullLoader)

        vendors = list(inventory.keys())

        for vendor in vendors:
            for host in list(inventory[vendor].keys()):
                host_ks = list(inventory[vendor][host].keys())
                if 'password' not in host_ks:
                    inventory[vendor][host]['password'] = os.getenv('{}_PASSWORD'.format(str(vendor).upper()))

                for key in list(self.defaults[vendor].keys()):
                    inventory[vendor][host][key] = self.defaults[vendor][key]

        return inventory

    def probe(self, vendor):
        vendor_interface = INTERFACES[vendor](self.inventory[vendor])
        vendor_interface.probe_inventory_ssl_cert_list()
        self.inventory[vendor] = vendor_interface.inventory

    def persist_data(self):
        # ensuring database creation
        self.db['base'].metadata.create_all(self.db['engine'])

        for vendor in self.inventory:
            for host in self.inventory[vendor]:
                for cert in self.inventory[vendor][host]['ssl_certs']:
                    cert_name = list(cert.keys())[0]
                    cert_data = {
                        'hostname': host,
                        'ip': heimdall.inventory[vendor][host]['ip'],
                        'cert_name': cert_name,
                        'vendor': vendor,
                        'expires_at': cert[cert_name]['expires_at']
                    }
                    this_certificate = Certificate(**cert_data)
                    this_certificate.save(session)

    def notify_expired(self):
        """
        here whistleblower is called to check which notifications need to be raised, if any.
        DB session is closed within this method, therefore it MUST be the last one to be called.
        """

        whistleblower = Whistleblower(self.db['session'])
        whistleblower.notify()
        self.db['session'].close()


if __name__ == "__main__":
    try:
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s [%(levelname)s] %(message)s",
            handlers=[
                logging.FileHandler("heimdall.log"),
                logging.StreamHandler()
            ],
        )

        heimdall = Heimdall()
        for vendor in INTERFACES:
            heimdall.probe(vendor)

    except Exception as e:
        logging.exception('something turned sour...')
    finally:
        logging.info(re.sub(r'"password": .+,', '"password": <redacted>', json.dumps(heimdall.inventory, indent=2, default=str)))
        logging.info('Persisting probed certificates to database...')
        heimdall.persist_data()

        # TODO: there should be a cleaning step to remove records related to IPs no longer listed in hosts.yaml
        heimdall.notify_expired()
import sqlalchemy as sa
from settings import sql_alchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

engine = sa.create_engine(sql_alchemy.get('db_uri'))
session = scoped_session(sessionmaker(bind=engine))
Base = declarative_base()
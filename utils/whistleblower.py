import logging
import subprocess
from settings import notifications
from datetime import datetime
from models.certificate import Certificate



class Whistleblower(object):
    TODAY = datetime.now().date()

    def __init__(self, session):
        self.session = session
        self.notify_certs = Certificate.find_by_notify_date(session=self.session, date=Whistleblower.TODAY)

    def notify(self):
        for cert in self.notify_certs:
            logging.info("Notifying {} - {}".format(cert.ip, cert.cert_name))

            if not cert.last_notified_at:
                days_until_notification = notification_proc['notify_delta']
                notification_proc = subprocess.Popen(["/opt/cnmc/eircom/cell-messenger/bin/cell-event",
                                                      "{}".format(notifications['severity'][days_until_notification]),
                                                      "SSL Cert Validation",
                                                      "Cert is due to expire in {} days".format(days_until_notification)],
                                                     stdout=subprocess.PIPE,
                                                     stderr=subprocess.PIPE)

                out, err = notification_proc.communicate()
                logging.info('Calling notification script:\n{}'.format(out))
                cert.last_notified_at = Whistleblower.TODAY
                self.session.commit()
        else:
            logging.info("Nothing to Notify")

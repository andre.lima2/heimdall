import logging
from utils.db import sa, Base
from settings import notifications
from datetime import timedelta, datetime


class Certificate(Base):

    # Constant defining how many days before the expiration of a certificate we need to raise a notification
    NOTIFY_DELTA = notifications.get('notify_delta')

    __tablename__ = "certificates"

    hostname = sa.Column(sa.String, nullable=False)
    ip = sa.Column(sa.String, index=True)
    cert_name = sa.Column(sa.String, index=True)
    vendor = sa.Column(sa.String, nullable=False)
    expires_at = sa.Column(sa.Date, nullable=False)
    notify_at = sa.Column(sa.Date, nullable=False)
    last_verified_at = sa.Column(sa.DateTime, nullable=False)
    last_notified_at = sa.Column(sa.Date, nullable=True)

    # This table key's is a composite of ip + cert_name
    __table_args__ = (
        sa.PrimaryKeyConstraint(
            ip, cert_name
        ),
    )

    def __init__(self, hostname, ip, cert_name, vendor, expires_at):
        self.hostname = hostname
        self.ip = ip
        self.cert_name = cert_name
        self.vendor = vendor
        self.expires_at = expires_at
        self.notify_at = self.expires_at - timedelta(Certificate.NOTIFY_DELTA)
        self.last_verified_at = datetime.now()

    @classmethod
    def find_by_notify_date(cls, session, date):
        return session.query(cls).filter(cls.notify_at <= date).all()

    def save(self, session):
        """
        Checks if a record with same 'ip' and 'cert_name' already exist. If it does, updates that record.
        Otherwise, creates a new record.

        :return: the resulting certificate record as a Certificate object.
        """
        # check if a record for 'ip' & 'cert_name' already exists
        cert = session.query(Certificate).filter_by(ip=self.ip, cert_name=self.cert_name).first()
        if cert:
            # updates the existing record's fields with current (self) object's fields - except for 'ip' & 'cert_name'
            cert.hostname = self.hostname
            cert.expires_at = self.expires_at
            cert.notify_at = self.notify_at
            cert.last_verified_at = self.last_verified_at
        # if no certificate exists with 'ip' and 'cert_name'
            logging.info("updating {} - {}".format(self.ip, self.cert_name))
        else:
            logging.info("creating {} - {}".format(self.ip, self.cert_name))
            session.add(self)
        session.commit()

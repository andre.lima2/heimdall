sql_alchemy = {
    'db_uri': 'sqlite:///data.db'
}

notifications = {
    'script': '/opt/cnmc/eircom/cell-messenger/bin/cell-event',
    'notify_delta': 30,
    'severity': {
        30: 'MAJOR',
        90: 'CRITICAL',
        180: 'WARN'
    }
}

multithreading = {
    'max_workers': 20
}